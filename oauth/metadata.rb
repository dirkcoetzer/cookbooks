name             'oauth'
maintainer       'Tribal Tactics'
maintainer_email 'dirkcoetzer@tribaltactics.biz'
license          'All rights reserved'
description      'Installs/Configures oauth'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'